﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NotesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private INoteCollectionService _noteCollectionService;
        private IOwnerCollectionService _ownerCollectionService;
        public OwnerController(INoteCollectionService noteCollectionService,
            IOwnerCollectionService ownerCollectionService)
        {
            _noteCollectionService = noteCollectionService;
            _ownerCollectionService = ownerCollectionService;
        }
        // GET: api/<OwnerController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _ownerCollectionService.GetAll());
        }

        //// GET api/<OwnerController>/5
        [HttpGet("{id}")]
        public IActionResult Get(ObjectId id)
        {
            return Ok(_ownerCollectionService.Get(id));
        }

        //// POST api/<OwnerController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Owner owner)
        {
            await _ownerCollectionService.Create(owner);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner([FromBody] Owner owner, [FromRoute] ObjectId id)
        {
            if (owner == null)
            {
                return BadRequest("Note cannot be null");
            }
            await _ownerCollectionService.Update(id, owner);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(ObjectId id)
        {
            bool deleted = await _ownerCollectionService.Delete(id);
            if (deleted == false)
            {
                return NotFound("Owner cannot be found");
            }
            return Ok();
        }

        //[HttpGet("{ownerId}/notes")]
        //public IActionResult GetNoteByOwnerId([FromRoute] Guid ownerId)
        //{
        //    return Ok(_noteCollectionService.GetNotesByOwnerId(ownerId));
        //}

        //[HttpPost("{ownerId}/notes")]
        //public IActionResult PostNoteByOwnerId([FromRoute] Guid ownerId, [FromBody] Note note)
        //{
        //    note.OwnerId = ownerId;
        //    _noteCollectionService.Create(note);
        //    return NoContent();
        //}

        //[HttpPut("{ownerId}/notes/{noteId}")]
        //public IActionResult PutNoteByOwnerId([FromRoute] Guid ownerId, [FromRoute] Guid noteId, [FromBody] Note note)
        //{
        //    if (note == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }
        //    note.Id = noteId;
        //    note.OwnerId = ownerId;
        //    _noteCollectionService.Update(noteId, note);
        //    return Ok();
        //}

        //[HttpDelete("{ownerId}/notes/{noteId}")]
        //public IActionResult DeleteNoteByOwnerId([FromRoute] Guid ownerId, [FromRoute] Guid noteId, [FromBody] Note note)
        //{
        //    bool deleted = _noteCollectionService.Delete(noteId);
        //    if (deleted == false)
        //    {
        //        return NotFound("Note cannot be found");
        //    }
        //    return Ok();
        //}

        //[HttpDelete("{ownerId}/notes")]
        //public IActionResult DeleteNotesOfOwner([FromRoute] Guid ownerId)
        //{
        //    _ownerCollectionService.DeleteNotesByOwner(ownerId);
        //    return Ok();
        //}
    }
}
