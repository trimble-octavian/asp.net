﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        INoteCollectionService _noteCollectionService;
        public NotesController(INoteCollectionService noteCollectionService) 
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService)); ;
        }

        [HttpGet]
        public async Task<IActionResult> GetNotes()
        {
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpGet("{Id}", Name = "GetNote")]
        public async Task<IActionResult> GetNoteById([FromRoute] ObjectId Id)
        {
            return Ok(await _noteCollectionService.Get(Id));
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Note note)
        {
            await _noteCollectionService.Create(note);
            return CreatedAtRoute("GetNote", new { Id = note.Id }, note);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateNote([FromRoute] ObjectId id, [FromBody] Note note)
        {
            if (note == null)
            {
                return BadRequest("Note cannot be null");
            }
            await _noteCollectionService.Update(id, note);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNote(ObjectId id)
        {
            bool deleted = await _noteCollectionService.Delete(id);
            if (deleted == false)
            {
                return NotFound("Note cannot be found");
            }
            return Ok();
        }
    }
}
