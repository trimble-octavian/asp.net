﻿using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NotesAPI.Controllers
{
    
    /// <summary>
    /// This controller is responsible with the CRUD operations asociated to the Category model.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private static ArrayList CategoriesCollection = new ArrayList()
        {
            new Category("To do", "1"),
            new Category("Done", "2"),
            new Category("Doing", "3")
        };

        // GET: api/<CategoriesController>
        [HttpGet]
        public ArrayList Get()
        {
            return CategoriesCollection;
        }

        // GET api/<CategoriesController>/5
        [HttpGet("{id}")]
        public Category Get(int id)
        {
            return (Category) CategoriesCollection[id];
        }

        // POST api/<CategoriesController>
        [HttpPost]
        public void Post([FromBody] Category category)
        {
            CategoriesCollection.Add(category);
        }

        // PUT api/<CategoriesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Category category)
        {
            CategoriesCollection[id] = category;
        }

        // DELETE api/<CategoriesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CategoriesCollection.RemoveAt(id);
        }
    }
}
