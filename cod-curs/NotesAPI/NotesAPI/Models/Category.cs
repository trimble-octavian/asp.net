﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Models
{
    public class Category
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Category()
        {

        }
        public Category(string name, string id)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}
