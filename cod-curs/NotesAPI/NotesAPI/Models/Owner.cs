﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Models
{
    public class Owner
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Owner()
        {

        }
        //public Owner(string name)
        //{
        //    this.Name = name;
        //    Id = Guid.NewGuid();
        //}
    }
}
