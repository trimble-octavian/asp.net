﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Models
{
    public class Note
    {
        public string CategoryId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        [BsonId]
        public ObjectId Id { get; set; }
        [Required] public ObjectId? OwnerId { get; set; }
    }
}
