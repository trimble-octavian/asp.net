﻿using MongoDB.Bson;
using MongoDB.Driver;
using NotesAPI.Models;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Services
{
    public class OwnerCollectionService : IOwnerCollectionService
    {
        private readonly IMongoCollection<Owner> _owners;

        public OwnerCollectionService(IMongoDBSettings mongoDBSettings)
        {
            var client = new MongoClient(mongoDBSettings.ConnectionString);
            var database = client.GetDatabase(mongoDBSettings.DatabaseName);

            _owners = database.GetCollection<Owner>(mongoDBSettings.OwnerCollectionName);
        }

        public async Task<bool> Create(Owner model)
        {
            await _owners.InsertOneAsync(model);
            return true;
        }

        public async Task<bool> Delete(ObjectId id)
        {
            var result = await _owners.DeleteOneAsync(Owner => Owner.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }


        public async Task<Owner> Get(ObjectId id)
        {
            return (await _owners.FindAsync(Owner => Owner.Id == id)).FirstOrDefault();
        }


        public async Task<List<Owner>> GetAll()
        {
            var result = await _owners.FindAsync(Owner => true);
            return result.ToList();
        }

        public async Task<List<Owner>> GetNotesByOwnerId(ObjectId ownerId)
        {
            return (await _owners.FindAsync(Owner => Owner.Id == ownerId)).ToList();
        }

        public async Task<bool> Update(ObjectId id, Owner Owner)
        {
            Owner.Id = id;
            var result = await _owners.ReplaceOneAsync(Owner => Owner.Id == id, Owner);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _owners.InsertOneAsync(Owner);
                return false;
            }

            return true;
        }

        public async Task<bool> DeleteNotesByOwner(ObjectId id)
        {
            var result = await _owners.DeleteManyAsync(Owner => Owner.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }
    }
}
