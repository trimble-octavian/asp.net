﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Services
{
    public interface ICollectionService<T>
    {
        Task<List<T>> GetAll();

        Task<T> Get(ObjectId id);

        Task<bool> Create(T model);

        Task<bool> Update(ObjectId id, T model);

        Task<bool> Delete(ObjectId id);
    }
}
